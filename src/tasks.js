// --------------------------------- 1
const getChars = array => {
    let str = '';
    array.map((word, i) => str += word.charAt(i));
    return str
}

// --------------------------------- 2
const char_freq = string => {
    let chars = {};
    string.split('').map(letter => {
        !chars[letter] ? chars[letter] = 1 : chars[letter] += 1;
    })
    return chars;
}

// --------------------------------- 3
const range = (start, end) => {
    let arr = [];
    for(let i = start; i <= end; i++){
      arr.push(i);
    }
    return arr.join(',');
  }

// --------------------------------- 4
const listNames = users => {
    let namesList = '';
    users.map(({name}, i) => {
        if (users.length > 1) {
            if (users.length > i + 1) {
                i === users.length - 2 ? namesList += `${name} ` : namesList += `${name}, `
            } else {
                namesList += `and ${name}`
            }
        } else {
            namesList += name
        }
    })
    return namesList
}

// --------------------------------- 5
function keysAndValues(obj) {
    const values = [];
    const keys = Object.keys(obj);
    keys.forEach((key) => {
      values.push(obj[key]);
    });
    return [keys, values];
  }

// --------------------------------- 6

const swapMinMax = (array) => { 
    let indexMin = array.indexOf(Math.min(...array));
    let indexMax = array.indexOf(Math.max(...array));
    array[indexMin] = array.splice(indexMax, 1, array[indexMin])[0];
    return array;
  }  

// --------------------------------- 7
const fizzBuzz = (start, end) => {
    for (let i = start; i <= end; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            console.log('FizzBuzz')
        } else if (i % 3 === 0) {
            console.log('Fizz');
        } else if (i % 5 === 0) {
            console.log('Buzz');
        } else {
            console.log(i)
        }
    }
}

// --------------------------------- 8
function add(n) {
    return function (b) {
      return n + b;
    };
  }

// --------------------------------- 9

const multiplyArrayItems = (array, multFactor) => {
    return array.map((item) => {
      return item * multFactor;
    })
  }  

// --------------------------------- 10
const uniqueValues = arr => {
    return [...new Set(arr)];
}



module.exports = {
    getChars,
    char_freq,
    range,
    listNames,
    keysAndValues,
    swapMinMax,
    fizzBuzz,
    add,
    multiplyArrayItems,
    uniqueValues
}