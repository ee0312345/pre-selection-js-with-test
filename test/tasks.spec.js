const { getChars, char_freq, range, 
        listNames, keysAndValues, swapMinMax, 
        fizzBuzz, add, multiplyArrayItems, uniqueValues } = require('../src/tasks')

require('mocha-sinon');

// --------------------------------------------- 1
describe('getChars function testing', () => {
    it('Should return "js3"', () => {
        assert.equal(getChars(['javascript', 'is cool', '123']), 'js3')
    });
    it('11-th commandment: You shall not hardcode!', () => {
        assert.equal(getChars(['python', 'is great', 'too', 'lalala']), 'psoa')
    });
});

// --------------------------------------------- 2
describe('char_freq function testing', () => {
    it('Should return { a: 7, b: 14, c: 3, d: 3 }', () => {
        assert.deepEqual(char_freq('abbabcbdbabdbdbabababcbcbab'), {a: 7, b: 14, c: 3, d: 3})
    });
    it('11-th commandment: You shall not hardcode!', () => {
        assert.deepEqual(char_freq('xbxabcbvvabdbvbabxbebxbcbabxev'), {x: 5, b: 12, a: 4, c: 2, v: 4, d: 1, e: 2})
    });
});

// --------------------------------------------- 3
describe('range function testing', () => {
    it('Should return correct string range according to arguments', () => {
      expect(range(1,10))
          .to.equal("1,2,3,4,5,6,7,8,9,10");
    });
    it('Should return correct string range according to arguments', () => {
      expect(range(4,6))
          .to.equal("4,5,6");
    });
    it('Should return correct string range according to arguments', () => {
      expect(range(1,2))
          .to.equal("1,2");
    });
    it('Should return correct string range according to arguments', () => {
      expect(range(66,77))
          .to.equal("66,67,68,69,70,71,72,73,74,75,76,77");
    });
    it('Should return correct string range according to arguments', () => {
      expect(range(0,8))
          .to.equal("0,1,2,3,4,5,6,7,8");
    });
});

// --------------------------------------------- 4
describe('listNames', () => {
    it('Should return "Max, Denis and Maria"', () => {
        assert.equal(listNames([{name: 'Max'}, {name: 'Denis'}, {name: 'Maria'}]), 'Max, Denis and Maria')
    });
    it('Should return "Max and Maria"', () => {
        assert.equal(listNames([{name: 'Max'}, {name: 'Maria'}]), 'Max and Maria')
    });
    it('Should return "Maria"', () => {
        assert.equal(listNames([{name: 'Maria'}]), 'Maria')
    });
    it('11-th commandment: You shall not hardcode!', () => {
        assert.equal(listNames([
            {name: 'Max'},
            {name: 'Denis'},
            {name: 'Maria'},
            {name: 'Varvara'},
            {name: 'Tychon'}
        ]), 'Max, Denis, Maria, Varvara and Tychon')
    });
});

// --------------------------------------------- 5
describe('keysAndValues function testing', () => {
    it('should return an array of 2 arrays', () => {
      const obj1 = { a: 1, b: 2, c: 3 };
      expect(keysAndValues(obj1)).to.deep.equal([['a', 'b', 'c'], [1, 2, 3]]);
    });
    it('should return an array of 2 arrays', () => {
      const obj1 = {
        a: 1, b: 2, c: 3, d: 7,
      };
      expect(keysAndValues(obj1)).to.deep.equal([['a', 'b', 'c', 'd'], [1, 2, 3, 7]]);
    });
    it('should return an array of 2 arrays', () => {
      const obj1 = {
        a: 133, b: 2, c: 3, d: 7,
      };
      expect(keysAndValues(obj1)).to.deep.equal([['a', 'b', 'c', 'd'], [133, 2, 3, 7]]);
    });
    it('should return an array of 2 arrays', () => {
      const obj1 = {
        a: 1, b: 2333, c: 3, d: 7,
      };
      expect(keysAndValues(obj1)).to.deep.equal([['a', 'b', 'c', 'd'], [1, 2333, 3, 7]]);
    });
});

// --------------------------------------------- 6
describe('swapMinMax function testing', () => {
    it('Should return correct array according to arguments', () => {
      expect(swapMinMax([5,3,8,1,6,3,9,2]))
          .to.eql([5,3,8,9,6,3,1,2]);
    });
    it('Should return correct array according to arguments', () => {
      expect(swapMinMax([15, 22]))
          .to.eql([22, 15]);
    });
    it('Should return correct array according to arguments', () => {
      expect(swapMinMax([55,14,21,23,56,77]))
          .to.eql([55,77,21,23,56,14]);
    });
    it('Should return correct array according to arguments', () => {
      expect(swapMinMax([5,1,3]))
          .to.eql([1,5,3]);
    });
    it('Should return correct array according to arguments', () => {
      expect(swapMinMax([7,6,7,8,11,11,11,14,2]))
          .to.eql([7,6,7,8,11,11,11,2,14]);
    });
    it('Should return correct array according to arguments', () => {
      expect(swapMinMax([2,2,5,5]))
          .to.eql([5,2,2,5]);
    });
});

// --------------------------------------------- 7
describe('FizzBuzz function testing', () => {

    beforeEach(function () {
        this.sinon.stub(console, 'log');
    });

    it('Should console.log: 1 2 "Fizz" 4', () => {
        fizzBuzz(1, 4);
        expect(console.log.callCount).to.be.equal(4);
        expect(console.log.calledWith(1)).to.be.true;
        expect(console.log.calledWith(2)).to.be.true;
        expect(console.log.calledWith('Fizz')).to.be.true;
        expect(console.log.calledWith(4)).to.be.true;
    })

    it('Should console.log: 7 8 "Fizz" "Buzz" 11 "Fizz" 13 14 "FizzBuzz"', () => {
        fizzBuzz(7, 15);
        expect(console.log.callCount).to.be.equal(9);
        expect(console.log.calledWith(7)).to.be.true;
        expect(console.log.calledWith(8)).to.be.true;
        expect(console.log.calledWith('Fizz')).to.be.true;
        expect(console.log.calledWith('Buzz')).to.be.true;
        expect(console.log.calledWith(11)).to.be.true;
        expect(console.log.calledWith('Fizz')).to.be.true;
        expect(console.log.calledWith(13)).to.be.true;
        expect(console.log.calledWith(14)).to.be.true;
        expect(console.log.calledWith('FizzBuzz')).to.be.true;
    })

    it('11-th commandment: You shall not hardcode!', () => {
        fizzBuzz(25, 30);
        expect(console.log.callCount).to.be.equal(6);
        expect(console.log.calledWith('Buzz')).to.be.true;
        expect(console.log.calledWith(26)).to.be.true;
        expect(console.log.calledWith('Fizz')).to.be.true;
        expect(console.log.calledWith(28)).to.be.true;
        expect(console.log.calledWith(29)).to.be.true;
        expect(console.log.calledWith('FizzBuzz')).to.be.true;
    })
});

// --------------------------------------------- 8
describe('Add Function', () => {
    it('add funсtion should return correct value ', () => {
      const addOne = add(1);
      expect(addOne(3)).to.equal(4);
    });
    it('add funсtion should return correct value ', () => {
      const addOne = add(2);
      expect(addOne(4)).to.equal(6);
    });
    it('add funсtion should return correct value ', () => {
      const addOne = add(10);
      expect(addOne(30)).to.equal(40);
    });
});

// --------------------------------------------- 9
describe('multiplyArrayItems function testing', () => {
    it('Should return correct array according to arguments', () => {
      expect(multiplyArrayItems([1, 10, -50, 0.5], 2))
          .to.eql([2, 20, -100, 1]);
    });
    it('Should return correct array according to arguments', () => {
      expect(multiplyArrayItems([1, 10, -50, 0.5], -10))
          .to.eql([-10, -100, 500, -5]);
    });
    it('Should return correct array according to arguments', () => {
      expect(multiplyArrayItems([4, 4, 4], 4))
          .to.eql([16,16,16]);
    });
    it('Should return correct array according to arguments', () => {
      expect(multiplyArrayItems([2, 12, -2, -4, 5, 11, 5, 8], -3))
          .to.eql([-6, -36, 6, 12, -15, -33,-15,-24]);
    });
    it('Should return correct array according to arguments', () => {
      expect(multiplyArrayItems([33, 55], 1))
          .to.eql([33,55]);
    });
  });

// --------------------------------------------- 10
describe('uniqueValues function testing', () => {
  it('Should return [33, 12]', () => {
    expect(uniqueValues([33, 33, 12]))
        .to.eql([33,12]);
  });
  it('Should return [22, 11, 23, 24, 25 ,35]', () => {
    expect(uniqueValues([22, 11, 23, 24, 25 ,35]))
        .to.eql([22, 11, 23, 24, 25 ,35]);
  });
  it('Should return [15, 5]', () => {
    expect(uniqueValues([15, 5, 5, 15]))
        .to.eql([15, 5]);
  });
  it('Should return [2, 1, 13, 11]', () => {
    expect(uniqueValues([2, 2, 2, 2, 1, 1, 1, 13, 11]))
        .to.eql([2,1,13,11]);
  });
  it('Should return [5, 4, 3, 11, 44]', () => {
    expect(uniqueValues([5, 5, 5, 5, 4, 4, 4, 3, 11, 44, 5]))
        .to.eql([5, 4, 3, 11, 44]);
  });
  it('11-th commandment: You shall not hardcode!', () => {
    expect(uniqueValues([44, 44, 99, 5, 2, 8, 2, 7, 9]))
        .to.eql([44, 99, 5, 2, 8, 7, 9]);
  });
});